import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    width: 200,
    height: 120,
    backgroundColor: '#131a21',
    borderWidth: 1,
    borderColor: '#3d495caa',
    borderStyle: 'solid',
    borderRadius: 5,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'absolute',
    cursor: 'pointer',
    zIndex: 1,
    // justifyContent: 'center',
    marginLeft: 30
  },
  image: {
    width: 80,
    height: 80,
    marginTop: 20,
    marginBottom: 35,
    zIndex: 1,
    borderRadius: 100,
    transition: '.5s',
    '&:hover': {
      transform: 'scale(1.3)',
      transition: '.5s'
    }
  },
  subTitle: {
    marginTop: 10,
    color: '#6ca9bd'
  },
  subTitle2: {
    marginTop: 10,
    color: '#6ca9bd',
    transition: '.5s',
    '&:hover': {
      transform: 'scale(1.2)',
      transition: '.5s',
      color: theme.palette.secondary.main
    }
  },
  pointDiv: {
    position: 'absolute',
    top: 2,
    right: 2,
    width: 30,
    height: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#1c4a20' ,
    backgroundColor: '#102344',
    borderRadius: 40,
    opacity: 0.7
  },
  title: { marginTop: 108 },
  topDiv: {
    width: 240,
    height: 250,
    // backgroundColor: '#131a21',
    backgroundColor: '#1e252b',
    borderWidth: 1,
    borderColor: '#3d495caa',
    borderStyle: 'solid',
    borderRadius: 5,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative',
    cursor: 'pointer',
    marginTop: 30,
    marginLeft: 10
    // zIndex: 1
    // top: -5
  },
  parent: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative'
  },
  priceDiv: {
    width: 260,
    height: 50,
    backgroundColor: '#131a21',
    borderWidth: 1,
    borderColor: '#3d495caa',
    borderStyle: 'solid',
    borderRadius: 5,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'absolute',
    cursor: 'pointer',
    zIndex: 1,
    bottom: 20
  }
}))
