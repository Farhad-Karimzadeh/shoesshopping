import { makeStyles } from '@material-ui/core'
export default makeStyles((theme) => ({
  topGrid: {
    marginTop: 10,
    marginBottom: 10,
    // marginLeft: 5,
    marginRight: 5,
    [theme.breakpoints.up('md')]: {
      height: 500
    }
  },
  sliderGrid: {
    height: 500,
    padding: 10,
    paddingLeft: 0,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 10,
      height: 300,
      paddingRight: 0
    }
  },
  catGrid: {
    height: 500,
    padding: 10,
    paddingRight: 0,
    [theme.breakpoints.down('xs')]: {
      height: 'auto'
      // paddingRight: 10
    }

    // border: '1px solid #000'
  },
  itemsGrid: {
    height: 180,
    marginTop: 10,
    marginBottom: 10,
    border: '1px solid #000'
  }
}))
