import {
  Modal,
  TextField,
  makeStyles,
  Typography,
  Button
} from '@material-ui/core'
import React, { useContext } from 'react'
import useStyles from './index.style'
import { LoginContext } from '../../contexts/LoginContext'
import { SIGNIN } from '../../constants/ActionType'

const makeTextfieldStyles = makeStyles({
  underLine: {
    '&&&:before': {
      borderBottom: 'none'
    },
    '&&:after': {
      borderBottom: 'none'
    }
  }
})

export default function ChangePasswordModal() {
  const { state, dispatch } = useContext(LoginContext)
  const classes = useStyles()
  const underLineStyles = makeTextfieldStyles()

  return (
    <Modal
      open={state.changePassword}
      onClose={() => dispatch({ type: 'closeAll' })}
    >
      <div className={classes.root}>
        <Typography variant="body2">رمز ورود جدید:</Typography>
        <TextField
          classes={{ root: classes.inputContainer }}
          variant="filled"
          fullWidth
          size="small"
          inputProps={{ className: classes.input }}
          InputProps={{ classes: underLineStyles }}
          placeholder="***********"
        />
        <Typography variant="body2">تکرار رمز ورود جدید:</Typography>
        <TextField
          classes={{ root: classes.inputContainer }}
          variant="filled"
          fullWidth
          size="small"
          inputProps={{ className: classes.input }}
          InputProps={{ classes: underLineStyles }}
          placeholder="***********"
        />

        <Button
          className={classes.button}
          fullWidth
          variant="outlined"
          color="primary"
        >
          تغییر رمز ورود{' '}
        </Button>
        <Button
          classes={{ label: classes.buttonLabel }}
          className={classes.buttonTwo}
          fullWidth
          variant="text"
          color="primary"
          onClick={() => {
            dispatch({ type: SIGNIN })
          }}
        >
          بازگشت به فرم ورود{' '}
        </Button>
      </div>
    </Modal>
  )
}
