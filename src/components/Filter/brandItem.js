import { Typography } from '@material-ui/core'
import React, { useState } from 'react'
import useStyles from './brands.style'
import AddIcon from '@material-ui/icons/Add'
import CloseIcon from '@material-ui/icons/Close'

export default function BrandItem({ title }) {
  const [active, setActive] = useState(true)
  const classes = useStyles()
  return (
    <div
      onClick={() => {
        setActive(!active)
      }}
      className={classes.brandItemRoot}
      style={{ backgroundColor: active ? '#871f83' : 'transparent' }}
    >
      <Typography variant="body2">{title}</Typography>
      {active ? (
        <CloseIcon className={classes.addIcon} />
      ) : (
        <AddIcon className={classes.addIcon} />
      )}
    </div>
  )
}
