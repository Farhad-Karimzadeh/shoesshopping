import { Grid, makeStyles } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import Item from './Item'
import ItemLoader from './ItemLoader'

const useStyles = makeStyles((theme) => ({
  root: {
    msOverflowStyle: 'none',
    '&::-webkit-scrollbar': {
      display: 'none'
    },
    [theme.breakpoints.down('xs')]: {
      flexWrap: 'nowrap',
      overflowX: 'scroll'
    }
  }
}))

export default function Womens() {
  const [loading, setLoading] = useState(true)
  const classes = useStyles()
  useEffect(() => {
    setTimeout(() => {
      setLoading(false)
    }, 10000)
  }, [])
  return (
    <Grid
      container
      item
      xs={12}
      direction="row"
      alignItems="center"
      justify="space-between"
      className={classes.root}
    >
      {loading ? (
        <>
          <ItemLoader />
          <ItemLoader />
          <ItemLoader />
          <ItemLoader />
          <ItemLoader />
        </>
      ) : (
        <>
          <Item />
          <Item />
          <Item />
          <Item />
          <Item />
        </>
      )}
    </Grid>
  )
}
