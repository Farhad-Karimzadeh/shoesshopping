import { Container, Grid } from '@material-ui/core'
import React from 'react'
import useStyles from './index.style'
import FilterList from '../../components/Filter'

export default function Index() {
  const classes = useStyles()
  return (
    <Container maxWidth="lg">
      <Grid container className={classes.root} dirction="row">
        <Grid className={classes.filterGrid} item lg={3}>
          <FilterList />
        </Grid>
        <Grid className={classes.contentGrid} item lg={9}></Grid>
      </Grid>
    </Container>
  )
}
