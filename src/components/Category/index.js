import { Typography, Button } from '@material-ui/core'
import React from 'react'
import useStyles from './index.styles'
import S1 from '../../assets/images/s1.png'
import S2 from '../../assets/images/s2.jpg'
import S3 from '../../assets/images/s3.jpg'
export default function Index() {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <div className={classes.catDiv}>
        <img src={S1} alt={'this men shoes'} className={classes.image} />
        <Typography variant="h5 ">کفش مردانه</Typography>
        <div className={classes.ButtonDiv}>
          <Button variant="outlined" color="secondary">
            <Typography className={classes.titleButton} variant="caption">
              مشاهده کفش ها
            </Typography>
          </Button>
        </div>
      </div>
      <div className={classes.catDiv}>
        <img src={S2} alt={'this men shoes'} className={classes.image} />
        <Typography variant="h5 ">کفش زنانه</Typography>
        <div className={classes.ButtonDiv}>
          <Button variant="outlined" color="secondary">
            <Typography className={classes.titleButton} variant="caption">
              مشاهده کفش ها
            </Typography>
          </Button>
        </div>
      </div>
      <div className={classes.catDiv}>
        <img src={S3} alt={'this men shoes'} className={classes.image} />
        <Typography variant="h5 ">کفش بچگانه</Typography>
        <div className={classes.ButtonDiv}>
          <Button variant="outlined" color="secondary">
            <Typography className={classes.titleButton} variant="caption">
              مشاهده کفش ها
            </Typography>
          </Button>
        </div>
      </div>
      <div className={classes.catDiv}>
        <img src={S3} alt={'this men shoes'} className={classes.image} />
        <Typography variant="h5 ">همه دسته ها</Typography>
        <div className={classes.ButtonDiv}>
          <Button variant="outlined" color="secondary">
            <Typography className={classes.titleButton} variant="caption">
              مشاهده کفش ها
            </Typography>
          </Button>
        </div>
      </div>
    </div>
  )
}
