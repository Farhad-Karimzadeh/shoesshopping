import React from 'react'
import useStyles from './brands.style'
import BrandItem from './brandItem'

export default function Brands() {
  const classes = useStyles()
  const brands = ['آدیداس', 'نایک', 'پوما', 'تن تاک']
  return (
    <div className={classes.root}>
      {brands.map((brand) => (
        <BrandItem key={brand} title={brand} />
      ))}
    </div>
  )
}
