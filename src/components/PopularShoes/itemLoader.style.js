import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    width: 240,
    height: 270,
    backgroundColor: '#131a21',
    borderWidth: 1,
    borderColor: '#3d495caa',
    borderStyle: 'solid',
    borderRadius: 5,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative',
    [theme.breakpoints.down('xs')]: {
      margin: 4,
      padding: 20,
      height: 260
    }
    //   cursor: 'pointer'
  },
  imageLoader: {
    width: '50%',
    marginTop: 25,
    backgroundColor: 'rgba(50,53,134,0.28)',
    [theme.breakpoints.down('xs')]: {
      width: 120,
      height: 120,
      marginTop: 15
    }
    // borderRadius: 5
  },
  textLoader: {
    width: '50%',
    marginTop: 15,
    backgroundColor: 'rgba(50,53,134,0.28)',

    borderRadius: 5
  },
  subTitleLoader: {
    width: '60%',
    marginTop: 10,
    backgroundColor: 'rgba(50,53,134,0.28)',
    borderRadius: 5
  },
  priceLoader: {
    width: '30%',
    marginTop: 10,
    backgroundColor: 'rgba(50,53,134,0.28)',
    borderRadius: 5
  }
  // image: {
  //   width: '70%',
  //   transition: '.5s',
  //   marginBottom: 35,
  //   zIndex: 1,
  //   '&:hover': {
  //     transform: 'scale(1.3)',
  //     transition: '.5s'
  //   }
  // },
  // subTitle: {
  //   marginTop: 10,
  //   color: '#6ca9bd'
  // },
  // pointDiv: {
  //   position: 'absolute',
  //   top: 2,
  //   right: 2,
  //   width: 30,
  //   height: 30,
  //   display: 'flex',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   // backgroundColor: '#1c4a20' ,
  //   backgroundColor: '#102344',
  //   borderRadius: 40,
  //   opacity: 0.7
  // }
}))
