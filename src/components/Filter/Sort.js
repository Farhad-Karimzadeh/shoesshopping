import React, { useState } from 'react'
import useStyles from './sort.style'
import {
  FormControlLabel,
  Radio,
  RadioGroup,
  Typography,
  Collapse
} from '@material-ui/core'

export default function Sort() {
  const [open, setOpen] = useState(false)
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Collapse collapsedHeight={54} in={open}>
        <div
          className={classes.titleDiv}
          onClick={() => setOpen(!open)}
          style={{
            borderBottomLeftRadius: open ? 0 : 5,
            borderBottomRightRadius: open ? 0 : 5
          }}
        >
          <Typography>مرتب سازی براساس</Typography>
        </div>
        <div className={classes.itemsDiv}>
          <RadioGroup>
            <FormControlLabel
              control={<Radio color="secondary" />}
              label="محبوب ترین"
              value="0"
            />
            <FormControlLabel
              control={<Radio color="secondary" />}
              label="ارزانترین"
              value="1"
            />
            <FormControlLabel
              control={<Radio color="secondary" />}
              label="گرانترین"
              value="2"
            />
          </RadioGroup>
        </div>
      </Collapse>
    </div>
  )
}
