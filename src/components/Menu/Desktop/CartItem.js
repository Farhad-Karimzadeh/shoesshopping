import { Typography } from '@material-ui/core'
import React from 'react'
import useStyles from './cartItem.style'
import DeleteIcon from '../../../assets/images/delete.svg'
import shoesImg from '../../../assets/images/shoes.png'

export default function CartItem() {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <div className={classes.rightContent}>
        <img src={shoesImg} alt="shoes" className={classes.shoesImg} />
        <div className={classes.descDiv}>
          <Typography variant="h6" color="textPrimary">
            کفش آدیداس
          </Typography>
          <div className={classes.deleteDiv}>
            <Typography variant="subtitle2" color="initial">
              300000
            </Typography>
            <img
              src={DeleteIcon}
              alt="shoseShopDeleteIcon"
              className={classes.deleteIcon}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
