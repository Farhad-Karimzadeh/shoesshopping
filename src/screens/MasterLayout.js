import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import useStyle from './MasterLayout.styles'
import DesktopMenu from '../components/Menu/Desktop'
import MobileMenu from '../components/Menu/Mobile'
import { Hidden } from '@material-ui/core'
import Footer from '../components/Footer'
import LoginModals from '../components/LoginModals'
import Routes from '../routes'

export default function MasterLayout() {
  const classes = useStyle()
  return (
    <BrowserRouter>
      <div className={classes.root}>
        <Hidden xsDown>
          <DesktopMenu />
        </Hidden>
        <Hidden smUp>
          <MobileMenu />
        </Hidden>
        <Routes />
        <Footer />
      </div>
      <LoginModals />
    </BrowserRouter>
  )
}
