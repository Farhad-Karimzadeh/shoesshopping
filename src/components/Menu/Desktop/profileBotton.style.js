import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 5,
    paddingLeft: 5,
    border: '1px #202d2a solid',
    height: '100%',
    width: 70,
    boxShadow: '1px 0px 3px 0px #3b3939'
  },
  topDiv: {
    cursor: 'pointer',
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transition: '.5s',
    ' &:hover': {
      opacity: 0.5,
      transition: '.5s'
    }
  },
  profileImg: {
    width: 35,
    borderRadius: 50,
    borderColor: theme.palette.secondary.main,
    borderWidth: 2,
    borderStyle: 'solid'
  },
  BottomIcon: {
    fontSize: 2,
    color: theme.palette.icon.shoppIcon
  },
  menuContent: {
    width: 200,
    backgroundColor: '#2a455f',
    position: 'absolute',
    top: 61,
    borderRadius: 5,
    padding: 15,
    boxShadow: '0px 0px 1px 0px #57b0d6',
    zIndex: 1
  }
}))
