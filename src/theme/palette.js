// eslint-disable-next-line import/no-anonymous-default-export
export default {
  primary: {
    main: '#367ef9',
    contrastText: '#fff'
  },
  secondary: {
    main: '#f06292',
    contrastText: '#FFF'
  },
  text: {
    primary: '#e3f0ed',
    secondary: '#586d71',
    disabled: '#b18080',
    title: '#3c4859',
    caption: '#dbe0e9',
    inavctiveMenu: '#959696'
  },
  icon: {
    shoppIcon: '#77a3bf'
  },
  border: {
    cartItemBorder: '#3e3f40',
    drawerItemBorder: '#393c41'
  }
}
