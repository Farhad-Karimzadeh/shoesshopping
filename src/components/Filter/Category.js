import React, { useState } from 'react'
import useStyles from './category.styles'
import {
  FormControlLabel,
  Checkbox,
  Typography,
  Collapse
} from '@material-ui/core'

export default function Category() {
  const [open, setOpen] = useState(false)
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Collapse collapsedHeight={54} in={open}>
        <div
          className={classes.titleDiv}
          onClick={() => setOpen(!open)}
          style={{
            borderBottomLeftRadius: open ? 0 : 5,
            borderBottomRightRadius: open ? 0 : 5
          }}
        >
          <Typography>دسته بندی ها</Typography>
        </div>
        <div className={classes.itemsDiv}>
          <FormControlLabel
            control={
              <Checkbox
                // checked={state.checkedB}
                // onChange={handleChange}
                name="checkedB"
                color="secondary"
              />
            }
            label="مردانه"
          />
          <FormControlLabel
            control={
              <Checkbox
                // checked={state.checkedB}
                // onChange={handleChange}
                name="checkedB"
                color="secondary"
              />
            }
            label="زنانه"
          />
          <FormControlLabel
            control={
              <Checkbox
                // checked={state.checkedB}
                // onChange={handleChange}
                name="checkedB"
                color="secondary"
              />
            }
            label="بچگانه"
          />
        </div>
      </Collapse>
    </div>
  )
}
