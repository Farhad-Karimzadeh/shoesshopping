import React from 'react'
import { Typography, Hidden } from '@material-ui/core'
import useStyles from './title.style'
import FilterImage from '../../assets/images/filter.svg'
import ExpandLessIcon from '@material-ui/icons/ExpandLess'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
export default function Title({ title, onClick, open }) {
  const classes = useStyles()
  return (
    <div className={classes.root} onClick={onClick}>
      <img className={classes.filterImage} src={FilterImage} alt={title} />
      <Typography className={classes.title} variant="h5">
        {title}
      </Typography>
      <Hidden smUp>
        {open ? (
          <ExpandLessIcon fontSize="large" />
        ) : (
          <ExpandMoreIcon fontSize="large" />
        )}
      </Hidden>
    </div>
  )
}
