import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 5,
    paddingLeft: 5,
    height: '100%',
    width: 70,
    border: '1px #202d2a solid',
    boxShadow: '1px 0px 3px 0px #3b3939'
  },
  shoppIcon: {
    width: 35
  },
  badge: {
    width: 15,
    height: 15,
    display: 'flex',
    backgroundColor: 'red',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: -5
  },
  CartContent: {
    width: 300,
    backgroundColor: '#2d3844',
    position: 'absolute',
    top: 61,
    right: 5,
    borderRadius: 5,
    padding: 15,
    boxShadow: '0px 0px 1px 0px #57b0d6',
    zIndex: 1
  },
  topDiv: {
    cursor: 'pointer',
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transition: '.5s',
    ' &:hover': {
      opacity: 0.5,
      transition: '.5s'
    },
    [theme.breakpoints.down('xs')]: {
      ' &:hover': {
        opacity: 1,
        transition: '.5s'
      }
    }
  },
  priceDiv: {
    padding: 10,
    borderBottomColor: theme.palette.border.cartItemBorder,
    borderBottomStyle: 'solid',
    borderBottomWidth: 1,
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  button: {
    marginTop: 10
  }
}))
