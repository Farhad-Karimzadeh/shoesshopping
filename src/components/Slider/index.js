import React from 'react'
import Carousel from 'react-material-ui-carousel'
import S1 from '../../assets/images/s1.png'
import S2 from '../../assets/images/s2.jpg'
import S3 from '../../assets/images/s3.jpg'
import Item from './Item'

//for change the left and right icon in carousel you should go to nodemadule react-material-ui-carousel\dist\components\carousel.js change the right to left and change the left to right
export default function Index() {
  let data = [
    {
      id: 1,
      title: 'کفش اول',
      image: S1,
      price: '5000000',
      subtitle: 'این زیر عنوان کفش است',
      discount: 7
    },
    {
      id: 2,
      title: 'کفش دوم',
      image: S2,
      price: '8000000',
      subtitle: 'این زیر عنوان کفش است',
      discount: 12
    },
    {
      id: 3,
      title: 'کفش سوم',
      image: S3,
      price: '2500000',
      subtitle: 'این زیر عنوان کفش است',
      discount: 15
    }
  ]
  return (
    <Carousel>
      {data.map((item) => (
        <Item key={item.id} item={item} />
      ))}
    </Carousel>
  )
}
