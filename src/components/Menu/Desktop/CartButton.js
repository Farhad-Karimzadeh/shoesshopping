import React, { useState } from 'react'
import useStyles from './cartBotton.style'
// import ShoppingBasketIcon from '@material-ui/icons/ShoppingCart'
import { Typography, Fade, Button } from '@material-ui/core'
import CartIcon from '../../../assets/images/cart.svg'
import CartItem from './CartItem'

export default function CartButton() {
  const classes = useStyles()
  const [open, setOpen] = useState(false)
  return (
    <div
      className={classes.root}
      onClick={() => setOpen(!open)}
      onMouseEnter={() => {
        setOpen(true)
      }}
      onMouseLeave={() => setOpen(false)}
    >
      <div className={classes.topDiv}>
        <img src={CartIcon} alt="Cart" className={classes.shoppIcon} />
        <div className={classes.badge}>
          <Typography variant="subtitle2" color="textPrimary">
            2
          </Typography>
        </div>
      </div>
      <Fade in={open}>
        <div className={classes.CartContent}>
          <CartItem />
          <CartItem />
          <CartItem />
          <div className={classes.priceDiv}>
            <Typography variant="body2" color="textPrimary">
              قیمت کل :
            </Typography>
            <Typography variant="h6" color="textPrimary">
              50.000
            </Typography>
          </div>
          <Button
            variant="outlined"
            color="primary"
            size="small"
            fullWidth
            className={classes.button}
          >
            نمایش سبد خرید
          </Button>
        </div>
      </Fade>
    </div>
  )
}
