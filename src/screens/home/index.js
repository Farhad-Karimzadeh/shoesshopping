import { Container, Grid } from '@material-ui/core'
import React from 'react'
import useStyles from './index.style'
import Silder from '../../components/Slider'
import Categories from '../../components/Category'
import PopularShoes from '../../components/PopularShoes'
import LazyLoad from 'react-lazyload'

export default function Index() {
  const classes = useStyles()
  return (
    <Container maxWidth="lg">
      <LazyLoad height={500}>
        <Grid className={classes.topGrid} direction="row" container>
          <Grid className={classes.sliderGrid} item lg={8} xs={12}>
            <Silder />
          </Grid>
          <Grid className={classes.catGrid} item lg={4} xs={12}>
            <Categories />
          </Grid>
        </Grid>
      </LazyLoad>
      <PopularShoes />
    </Container>
  )
}
