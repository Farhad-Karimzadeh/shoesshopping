import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 10
    // borderBottomStyle: 'solid',
    // borderBottomWidth: 1,
    // borderBottomColor: '#393c41'
    // paddingBottom: 10
    // paddingLeft: 10
  },
  titleDiv: {
    padding: 15,
    backgroundColor: '#41417f',
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    cursor: 'pointer'
  },
  itemsDiv: {
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    padding: 15,
    backgroundColor: '#11112d',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  btnDiv: {
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#11112d',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    paddingTop: 5
  }
}))
