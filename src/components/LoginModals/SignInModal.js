import {
  Modal,
  TextField,
  makeStyles,
  Typography,
  Button
} from '@material-ui/core'
import React, { useContext } from 'react'
import useStyles from './index.style'
import { LoginContext } from '../../contexts/LoginContext'
import { SIGNUP, FORGET_PASSWORD } from '../../constants/ActionType'
const makeTextfieldStyles = makeStyles({
  underLine: {
    '&&&:before': {
      borderBottom: 'none'
    },
    '&&:after': {
      borderBottom: 'none'
    }
  }
})

export default function SignInModal() {
  const { state, dispatch } = useContext(LoginContext)
  const classes = useStyles()
  const underLineStyles = makeTextfieldStyles()

  return (
    <Modal open={state.signin} onClose={() => dispatch({ type: 'closeAll' })}>
      <div className={classes.root}>
        <Typography variant="body2">تلفن همراه:</Typography>
        <TextField
          classes={{ root: classes.inputContainer }}
          variant="filled"
          fullWidth
          size="small"
          inputProps={{ className: classes.input }}
          InputProps={{ classes: underLineStyles }}
          placeholder="*********09"
        />
        <Typography variant="body2">رمز ورود:</Typography>
        <TextField
          classes={{ root: classes.inputContainer }}
          variant="filled"
          fullWidth
          size="medium"
          inputProps={{ className: classes.input }}
          InputProps={{ classes: underLineStyles }}
          placeholder="********"
        />
        <Button
          className={classes.button}
          fullWidth
          variant="outlined"
          color="primary"
        >
          ورود
        </Button>
        <Button
          classes={{ label: classes.buttonLabel }}
          className={classes.buttonTwo}
          fullWidth
          variant="text"
          color="primary"
          onClick={() => {
            dispatch({ type: SIGNUP })
          }}
        >
          ثبت نام نکرده اید؟
        </Button>
        <Button
          classes={{ label: classes.buttonLabel }}
          className={classes.buttonTwo}
          fullWidth
          variant="text"
          color="primary"
          onClick={() => {
            dispatch({ type: FORGET_PASSWORD })
          }}
        >
          رمز ورود خود را فراموش کرده اید؟
        </Button>
      </div>
    </Modal>
  )
}
