import { makeStyles } from '@material-ui/core'

export default makeStyles(() => ({
  root: {
    // backgroundColor: 'red'
  },
  image: {
    width: 88,
    height: 88,
    marginRight: 10
  },
  catDiv: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyItems: 'center',
    width: '100%',
    backgroundColor: '#131a21',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
    // boxShadow: '0px 0px 2px #504c4c'
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#3d495caa',
    position: 'relative'
    // marginTop:10
  },
  titleButton: {
    fontSize: 10
  },
  ButtonDiv: {
    position: 'absolute',
    right: 2,
    bottom: 2
  }
}))
