import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    width: '100%',
    height: 470,
    backgroundColor: '#011a1d',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    boxShadow: '0px 0px 2px #504c4c',
    [theme.breakpoints.down('xs')]: {
      height: 260
    }
  },
  image: {
    width: '80%'
  },
  rightSlider: {
    flex: 1,
    [theme.breakpoints.down('xs')]: {
      flex: 2
    }
  },
  leftSlider: {
    flex: 1,
    [theme.breakpoints.down('xs')]: {
      flex: 3
    }
  },
  subtitle: {
    marginTop: 15
  },
  discountDiv: {
    position: 'absolute',
    backgroundColor: '#811c6b',
    top: 0,
    left: 0,
    padding: 10,
    width: 100,
    borderBottomRightRadius: 100
    // marginTop: 0
    // height: 20
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  buttonDiv: {
    position: 'absolute',
    right: 5,
    bottom: 5
  }
}))
