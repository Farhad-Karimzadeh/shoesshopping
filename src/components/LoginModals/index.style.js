import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    width: 300,
    height: 'auto',
    backgroundColor: '#1a222a',
    margin: 'auto',
    marginTop: 80,
    padding: 25,
    '&:focus': {
      outline: 'none'
    },
    borderRadius: 5,
    [theme.breakpoints.down('xs')]: {
      width: '80%'
    }
  },
  rootSignUp: {
    width: 500,
    height: 'auto',
    backgroundColor: '#1a222a',
    margin: 'auto',
    marginTop: 80,
    padding: 25,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    '&:focus': {
      outline: 'none'
    },
    borderRadius: 5,

    [theme.breakpoints.down('xs')]: {
      width: '80%'
    }
  },
  inputContainer: {
    backgroundColor: '2a3a48',
    borderRadius: 5,
    marginTop: 10,
    marginBottom: 10,
    border: '1px solid #203d75'
  },
  input: {
    padding: 5,
    textAlign: 'right'
  },
  button: {
    marginTop: 15
  },
  buttonTwo: {
    textAlign: 'left',
    fontSize: 12,
    marginTop: 10
  },
  buttonLabel: {
    justifyContent: 'right'
  },
  formGrid: {
    paddingRight: 7,
    paddingLeft: 7,
    [theme.breakpoints.down('xs')]: {
      padding: 0
    }
  },
  codeDiv: {
    display: 'flex',
    flexDirection: 'row',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    marginBottom: 15
  },
  counterText: {
    textAlign: 'center',
    width: '100%',
    fontSize: 12,
    color: '#b3aeae'
  },
  buttonText: {
    fontSize: 12
  }
}))
