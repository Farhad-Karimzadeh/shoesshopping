import {
  Modal,
  TextField,
  makeStyles,
  Typography,
  Button
} from '@material-ui/core'
import React, { useContext } from 'react'
import useStyles from './index.style'
import { LoginContext } from '../../contexts/LoginContext'
import { SIGNUP, SIGNIN, CODE } from '../../constants/ActionType'
const makeTextfieldStyles = makeStyles({
  underLine: {
    '&&&:before': {
      borderBottom: 'none'
    },
    '&&:after': {
      borderBottom: 'none'
    }
  }
})

export default function ForgetPasswordModal() {
  const { state, dispatch } = useContext(LoginContext)
  // const [open, setOpen] = useState(false)
  const classes = useStyles()
  const underLineStyles = makeTextfieldStyles()
  const forgetPassword = () => {
    localStorage.setItem('loginCodeType', 'forgetPassword')
    dispatch({ type: CODE })
  }

  return (
    <Modal
      open={state.forgetPassword}
      onClose={() => dispatch({ type: 'closeAll' })}
    >
      <div className={classes.root}>
        <Typography variant="body2">تلفن همراه:</Typography>
        <TextField
          classes={{ root: classes.inputContainer }}
          variant="filled"
          fullWidth
          size="small"
          inputProps={{ className: classes.input }}
          InputProps={{ classes: underLineStyles }}
          placeholder="*********09"
        />

        <Button
          className={classes.button}
          fullWidth
          variant="outlined"
          color="primary"
          onClick={forgetPassword}
        >
          فراموشی رمز ورود
        </Button>
        <Button
          classes={{ label: classes.buttonLabel }}
          className={classes.buttonTwo}
          fullWidth
          variant="text"
          color="primary"
          onClick={() => {
            dispatch({ type: SIGNIN })
          }}
        >
          بازگشت به فرم ورود{' '}
        </Button>
        <Button
          classes={{ label: classes.buttonLabel }}
          className={classes.buttonTwo}
          fullWidth
          variant="text"
          color="primary"
          onClick={() => {
            dispatch({ type: SIGNUP })
          }}
        >
          ثبت نام
        </Button>
      </div>
    </Modal>
  )
}
