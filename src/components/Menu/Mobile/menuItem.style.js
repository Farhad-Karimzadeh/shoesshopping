import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    width: 250,
    height: '100%',
    backgroundColor: '#131a21',
    padding: 10
  },
  topDrawer: {
    width: '100%',
    height: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: theme.palette.border.drawerItemBorder,
    borderBottomStyle: 'solid',
    borderBottomWidth: 1,
    padding: 25
  },
  DrawerLogo: {
    width: 200
  },
  menuIcon: {
    width: 25,
    height: 25
  }
}))
