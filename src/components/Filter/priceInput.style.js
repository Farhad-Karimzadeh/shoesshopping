import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  input: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    flex: 1,
    marginRight: 5,
    // fontSize: 18,
    textAlign: 'center',
    color: '#fff',
    '&:focus': {
      outline: 'none'
    }
  },
  inputDiv: {
    padding: 10,
    borderWidth: '1px',
    borderStyle: 'solid',
    borderColor: '#44434a',
    alignItems: 'center',
    flex: 1,
    // marginRight: 10,
    borderRadius: 7,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
}))
