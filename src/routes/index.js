import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from '../screens/home'
import Search from '../screens/search'

export default function index() {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/search">
        <Search />
      </Route>
    </Switch>
  )
}
