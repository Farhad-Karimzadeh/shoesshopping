import React from 'react'
import useStyles from './item.style'
import S2 from '../../assets/images/s2.jpg'
import { Typography } from '@material-ui/core'

export default function Item() {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <img src={S2} alt="shoes" className={classes.image} />
      <Typography variant="h5">کفش دوم</Typography>
      <Typography variant="body2" className={classes.subTitle}>
        زیر عنوان کفش است
      </Typography>
      <Typography variant="body2" className={classes.subTitle}>
        70,000 تومان
      </Typography>
      <div className={classes.pointDiv}>
        <Typography color="text" variant="body2">
          4.9
        </Typography>
      </div>
    </div>
  )
}
