import React, { useState } from 'react'
import useStyles from './index.style'
import menuIcon from '../../../assets/images/menu.svg'
import textLogo from '../../../assets/images/logo.png'
import CartButton from '../Desktop/CartButton'
import Swipeable from '@material-ui/core/SwipeableDrawer'
import MenuItem from './MenuItem'
export default function Mobile() {
  const classes = useStyles()
  const [open, setOpen] = useState(false)
  const toggleOpenMenu = () => {
    setOpen(!open)
  }
  return (
    <div className={classes.root}>
      <div className={classes.rightMenu} onClick={toggleOpenMenu}>
        <img src={menuIcon} alt="shoes shop" className={classes.menuIcon} />
      </div>
      <div className={classes.centerMenu}>
        <img src={textLogo} alt="shoes shop" className={classes.textLogo} />
      </div>
      <div className={classes.leftMenu}>
        <CartButton />
      </div>
      <Swipeable open={open} onOpen={toggleOpenMenu} onClose={toggleOpenMenu}>
        <MenuItem />
      </Swipeable>
    </div>
  )
}
