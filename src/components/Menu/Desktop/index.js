import React from 'react'
import useStyles from './index.style'
import logo from '../../../assets/images/logo.png'
import { Button } from '@material-ui/core'
import { Link, useLocation } from 'react-router-dom'
import ProfileBTN from './ProfileButton'
import CartButton from './CartButton'

export default function Desktop() {
  const classes = useStyles()
  const location = useLocation()
  const path = location.pathname
  return (
    <div className={classes.root}>
      <div className={classes.logoDiv}>
        <img src={logo} alt="shoes shop" className={classes.logoImg} />
      </div>
      <div className={classes.menuDiv}>
        <div className={path === '/' ? classes.activeMenuDiv : null}>
          <Button
            className={path === '/' ? classes.activeMenu : classes.inactiveMenu}
            component={Link}
            to="/"
          >
            صفحه اصلی
          </Button>
        </div>
        <div className={path === '/faq' ? classes.activeMenuDiv : null}>
          <Button
            className={
              path === '/faq' ? classes.activeMenu : classes.inactiveMenu
            }
            component={Link}
            to="/faq"
          >
            سوالات پر تکرار
          </Button>
        </div>
        <div className={path === '/blog' ? classes.activeMenuDiv : null}>
          <Button
            className={
              path === '/blog' ? classes.activeMenu : classes.inactiveMenu
            }
            component={Link}
            to="/blog"
          >
            بلاگ{' '}
          </Button>
        </div>
        <div className={path === '/contact' ? classes.activeMenuDiv : null}>
          <Button
            className={
              path === '/contact' ? classes.activeMenu : classes.inactiveMenu
            }
            component={Link}
            to="/contact"
          >
            تماس با ما
          </Button>
        </div>
      </div>
      <div className={classes.leftMenuDiv}>
        <ProfileBTN />
        <CartButton />
      </div>
    </div>
  )
}
