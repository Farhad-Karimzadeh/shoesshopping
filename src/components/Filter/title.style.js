import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    width: '100%',
    height: 50,
    borderBottomColor: theme.palette.border.drawerItemBorder,
    borderBottomStyle: 'solid',
    borderBottomWidth: 2,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  filterImage: {
    width: 30,
    marginRight: 10
  },
  title: {
    flex: 1
  }
}))
