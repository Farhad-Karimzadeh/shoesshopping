import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
    borderBottomStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: '#393c41',
    paddingBottom: 10
  },
  brandItemRoot: {
    padding: 10,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 5,
    borderRadius: 5,
    cursor: 'pointer',
    border: '1px solid #2b2929',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: '1px 1px 5px 1px #5c5c5c'
  },
  addIcon: {
    marginLeft: 5,
    width: 20
  }
}))
