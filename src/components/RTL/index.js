import React from 'react'
import rtl from 'jss-rtl'
import { create } from 'jss'
import { StylesProvider, jssPreset } from '@material-ui/core/styles'

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] })

function RTL({ children }) {
  // eslint-disable-next-line react/jsx-filename-extension
  return <StylesProvider jss={jss}>{children}</StylesProvider>
}
export default RTL
