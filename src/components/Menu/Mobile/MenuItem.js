import React from 'react'
import useStyles from './menuItem.style'
import DrawerLogo from '../../../assets/images/logo.png'
import homeIcon from '../../../assets/images/home.svg'
import faqIcon from '../../../assets/images/faq.svg'
import blogIcon from '../../../assets/images/blog.svg'
import contactIcon from '../../../assets/images/contact.svg'
import userIcon from '../../../assets/images/user.svg'
import { Link, useLocation } from 'react-router-dom'
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography
} from '@material-ui/core'
export default function MenuItem() {
  const location = useLocation()
  const path = location.pathname
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <div className={classes.topDrawer}>
        <img src={DrawerLogo} alt="shoes shop" className={classes.DrawerLogo} />
      </div>
      <div>
        <List>
          <ListItem button component={Link} to="/">
            <ListItemIcon>
              <img
                src={homeIcon}
                alt="home of shoes shop"
                className={classes.menuIcon}
              />
            </ListItemIcon>
            <ListItemText>
              <Typography
                variant="body2"
                color={path === '/' ? 'primary' : 'textSecondary'}
              >
                صفحه اصلی
              </Typography>
            </ListItemText>
          </ListItem>
          <ListItem button component={Link} to="/faq">
            <ListItemIcon>
              <img
                src={faqIcon}
                alt="home of shoes shop"
                className={classes.menuIcon}
              />
            </ListItemIcon>
            <ListItemText>
              <Typography
                variant="body2"
                color={path === '/faq' ? 'primary' : 'textSecondary'}
              >
                سوالات متداول
              </Typography>
            </ListItemText>
          </ListItem>
          <ListItem button component={Link} to="/blog">
            <ListItemIcon>
              <img
                src={blogIcon}
                alt="home of shoes shop"
                className={classes.menuIcon}
              />
            </ListItemIcon>
            <ListItemText>
              <Typography
                variant="body2"
                color={path === '/blog' ? 'primary' : 'textSecondary'}
              >
                بلاگ
              </Typography>
            </ListItemText>
          </ListItem>
          <ListItem button component={Link} to="/contact">
            <ListItemIcon>
              <img
                src={contactIcon}
                alt="home of shoes shop"
                className={classes.menuIcon}
              />
            </ListItemIcon>
            <ListItemText>
              <Typography
                variant="body2"
                color={path === '/contact' ? 'primary' : 'textSecondary'}
              >
                تماس با ما{' '}
              </Typography>
            </ListItemText>
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <img
                src={userIcon}
                alt="home of shoes shop"
                className={classes.menuIcon}
              />
            </ListItemIcon>
            <ListItemText>
              <Typography variant="body2" color="textSecondary">
                ورود / ثبت نام
              </Typography>
            </ListItemText>
          </ListItem>
        </List>
      </div>
    </div>
  )
}
