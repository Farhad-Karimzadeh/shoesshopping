import React, { useState } from 'react'
import useStyles from './price.style'
import { Typography, Collapse, Button } from '@material-ui/core'
import PriceInput from './PriceInput'

export default function Price() {
  const [open, setOpen] = useState(false)
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Collapse collapsedHeight={54} in={open}>
        <div
          className={classes.titleDiv}
          onClick={() => setOpen(!open)}
          style={{
            borderBottomLeftRadius: open ? 0 : 5,
            borderBottomRightRadius: open ? 0 : 5
          }}
        >
          <Typography>تعیین محدوده قیمت</Typography>
        </div>
        <div className={classes.itemsDiv}>
          <PriceInput placeHolder="حداقل قیمت" />
          <PriceInput placeHolder="حداکثر قیمت" />
          <Button
            className={classes.priceBtn}
            variant="outlined"
            color="primary"
          >
            {' '}
            اعمال قیمت
          </Button>
        </div>
      </Collapse>
    </div>
  )
}
