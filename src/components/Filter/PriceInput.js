import { Typography } from '@material-ui/core'
import React, { useState } from 'react'
import useStyles from './priceInput.style'

export default function PriceInput({ placeHolder }) {
  const classes = useStyles()
  const [value, setValue] = useState('')

  const getPriceText = (val) => {
    let result = ''
    let j = 0

    for (let i = val.length; i > 0; i -= 1) {
      if (j === 3) {
        result = `,${result}`
        j = 0
      }

      result = val[i - 1] + result
      j += 1
    }
    console.log(result)
    return result
  }

  //   const [valueText, setValueText] = useState('')
  //   useEffect(() => {
  //     setValueText(`${value},`)
  //   }, [value])
  return (
    <div className={classes.root}>
      <div className={classes.inputDiv}>
        <input
          //   value={valueText}
          value={value}
          onChange={(e) => {
            //    setValue(e.target.value)
            const val = e.target.value
            setValue(getPriceText(val.replace(/,/g, '')))
          }}
          className={classes.input}
          placeholder={placeHolder}
        />
        <Typography variant="body2">تومان</Typography>
      </div>
    </div>
  )
}
