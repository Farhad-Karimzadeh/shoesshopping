import React from 'react'
import useStyles from './itemLoader.style'
import Skeleton from '@material-ui/lab/Skeleton'

export default function ItemLoader() {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Skeleton
        className={classes.imageLoader}
        variant="circle"
        // width={210}
        height={120}
        animation="pulse"
      />
      <Skeleton
        className={classes.textLoader}
        variant="text"
        // width={210}
        height={20}
        animation="pulse"
      />
      <Skeleton
        className={classes.subTitleLoader}
        variant="text"
        // width={210}
        height={15}
        animation="pulse"
      />
      <Skeleton
        className={classes.priceLoader}
        variant="text"
        // width={210}
        height={10}
        animation="pulse"
      />

      {/* <img src={S2} alt="shoes" className={classes.image} />
      <Typography variant="h5">کفش دوم</Typography>
      <Typography variant="body2" className={classes.subTitle}>
        زیر عنوان کفش است
      </Typography>
      <Typography variant="body2" className={classes.subTitle}>
        70,000 تومان
      </Typography>
      <div className={classes.pointDiv}>
        <Typography color="text" variant="body2">
          4.9
        </Typography>
      </div> */}
    </div>
  )
}
