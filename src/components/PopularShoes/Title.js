import React from 'react'
import { Grid, Typography, Button } from '@material-ui/core'
import useStyles from './title.style'

export default function Title({ title }) {
  const classes = useStyles()
  return (
    <Grid
      container
      item
      xs={12}
      direction="row"
      justify="space-between"
      alignItems="center"
      className={classes.root}
    >
      <Typography className={classes.title} variant="h5">
        {title}
      </Typography>
      <Button
        size="small"
        className={classes.button}
        variant="outlined"
        color="secondary"
      >
        مشاهده همه
      </Button>
    </Grid>
  )
}
