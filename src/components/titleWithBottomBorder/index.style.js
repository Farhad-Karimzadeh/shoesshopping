import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  titleDiv: {
    marginBottom: 10,
    paddingBottom: 20,
    position: 'relative',
    width: '100%',
    '&:after': {
      content: "''",
      position: 'absolute',
      width: 80,
      height: 4,
      borderRadius: 4,
      backgroundColor: '#00c1c9',
      bottom: 0,
      transmition: '5ms'
    },
    '&:before': {
      content: "''",
      position: 'absolute',
      width: 120,
      height: 4,
      borderRadius: 4,
      backgroundColor: '#bffcff',
      bottom: 0
    }
  }
}))
