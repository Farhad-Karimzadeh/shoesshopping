import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    padding: 10,
    borderBottomColor: theme.palette.border.cartItemBorder,
    borderBottomStyle: 'solid',
    borderBottomWidth: 1,
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  deleteIcon: {
    width: 18
  },
  shoesImg: {
    // borderRadius: 50,
    width: 60,
    backgroundColor: '#90caf9',
    borderRadius: 10,
    padding: 1
  },
  rightContent: {
    display: 'flex',
    flexDirection: 'row',
    // alignItems: 'center',
    width: '100%'
  },
  descDiv: {
    marginLeft: 10,
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
    // display: 'flex',
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // padding: 10
  },
  deleteDiv: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
}))
