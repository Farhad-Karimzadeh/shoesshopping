import { makeStyles } from '@material-ui/core'
export default makeStyles(() => ({
  root: {
    marginTop: 20,
    marginBottom: 20
  },
  filterGrid: {
    borderRadius: 10,
    backgroundColor: '#131a21',
    padding: 15
  },
  contentGrid: {
    height: 500
  }
}))
