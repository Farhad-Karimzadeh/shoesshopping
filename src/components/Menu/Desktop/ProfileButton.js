import React, { useState, useContext } from 'react'
import Avatar from '../../../assets/images/avatar.png'
import useStyles from './profileBotton.style'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { Button, Typography, Fade } from '@material-ui/core'
import { LoginContext } from '../../../contexts/LoginContext'
// import { SIGNIN, SIGNUP } from '../../../constants/ActionType'

export default function ProfileButton() {
  const { dispatch } = useContext(LoginContext)
  const classes = useStyles()
  const [open, setOpen] = useState(false)
  return (
    <div
      className={classes.root}
      onMouseEnter={() => {
        setOpen(true)
      }}
      onMouseLeave={() => setOpen(false)}
    >
      <div className={classes.topDiv} onClick={() => setOpen(!open)}>
        <ExpandMoreIcon className={classes.BottomIcon} />
        <img src={Avatar} alt="" className={classes.profileImg} />
      </div>
      {/* {open ? ( */}
      <Fade in={open}>
        <div className={classes.menuContent}>
          {localStorage.token ? (
            <Button>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                size="large"
              >
                <Typography variant="subtitle2">پروفایل</Typography>
              </Button>
              <Button
                variant="text"
                color="primary"
                fullWidth
                size="small"
                // style={{ marginTop: 10 }}
              >
                خروج
              </Button>
            </Button>
          ) : (
            <>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                size="medium"
                onClick={() => {
                  setOpen(false)
                  dispatch({ type: 'SIGNIN' })
                }}
              >
                ورود
              </Button>
              <Button
                variant="text"
                color="primary"
                fullWidth
                size="small"
                // style={{ marginTop: 10 }}
                onClick={() => {
                  setOpen(false)
                  dispatch({ type: 'SIGNUP' })
                }}
              >
                ثبت نام
              </Button>
            </>
          )}
        </div>
      </Fade>
      {/* ) : null} */}
    </div>
  )
}
