import React from 'react'
import useStyles from './test.style'
import S2 from '../../assets/images/s2.jpg'
import { Tooltip, Typography } from '@material-ui/core'

export default function Test() {
  const classes = useStyles()
  return (
    <div className={classes.parent}>
      <div className={classes.root}>
        <img src={S2} alt="shoes" className={classes.image} />
        <div className={classes.pointDiv}>
          <Typography color="text" variant="body2">
            4.9
          </Typography>
        </div>
      </div>
      <div className={classes.topDiv}>
        <Typography className={classes.title} variant="h5">
          کفش دوم
        </Typography>
        <Typography variant="body2" className={classes.subTitle}>
          زیر عنوان کفش است
        </Typography>
      </div>
      <Tooltip title="برای خرید فشار دهید" arrow>
        <div className={classes.priceDiv}>
          <Typography variant="h5" className={classes.subTitle2}>
            70,000 تومان
          </Typography>
        </div>
      </Tooltip>
    </div>
  )
}
