import { Modal, Typography, Button } from '@material-ui/core'
import React, { useState, useEffect, useContext } from 'react'
import useStyles from './index.style'
import ReactCodeInput from 'react-code-input'
import { LoginContext } from '../../contexts/LoginContext'
import {
  FORGET_PASSWORD,
  SIGNUP,
  // CODE,
  SIGNIN,
  CHANGE_PASSWORD
} from '../../constants/ActionType'

export default function CodeModal() {
  let timer = 120
  const { state, dispatch } = useContext(LoginContext)
  const [counter, setCounter] = useState(timer)
  let codeType = localStorage.getItem('loginCodeType')
  const classes = useStyles()
  useEffect(() => {
    setInterval(() => {
      setCounter((oldCounter) => (oldCounter === 0 ? 0 : oldCounter - 1))
    }, 1000)
  }, [])
  const checkCode = () => {
    if (codeType === 'signUp') {
      //
    } else {
      dispatch({ type: CHANGE_PASSWORD })
    }
  }
  const setCodeAgain = () => {
    //
    setCounter(timer)
  }

  return (
    <Modal open={state.code} onClose={() => dispatch({ type: 'closeAll' })}>
      <div className={classes.root}>
        <Typography variant="body2">
          کد ارسالی به تلفن همراهتان را در کادر زیر وارد کنید
        </Typography>
        <div className={classes.codeDiv} dir="ltr">
          <ReactCodeInput
            inputStyle={{
              backgroundColor: '#2a3a48',
              width: 40,
              height: 40,
              margin: 4,
              border: 'none',
              textAlign: 'center',
              color: 'white',
              fontSize: 18
              // borderRadius: 50
            }}
            fields={5}
          />
        </div>
        {counter !== 0 ? (
          <Typography variant="body2" className={classes.counterText}>
            {counter} {'  '} ثانیه تا پایان اعتبار کد
          </Typography>
        ) : (
          <Button
            fullWidth
            variant="text"
            color="primary"
            onClick={setCodeAgain}
          >
            <Typography variant="body2" className={classes.buttonText}>
              ارسال مجدد کد
            </Typography>
          </Button>
        )}
        <Button
          className={classes.button}
          fullWidth
          variant="outlined"
          color="primary"
          onClick={checkCode}
          disabled={counter === 0}
        >
          تایید کد
        </Button>
        <Button
          classes={{ label: classes.buttonLabel }}
          className={classes.buttonTwo}
          fullWidth
          variant="text"
          color="primary"
          onClick={() => {
            dispatch({ type: codeType === 'signUp' ? SIGNUP : FORGET_PASSWORD })
          }}
          disabled={counter ? true : false}
        >
          اصلاح شماره موبایل
        </Button>
        <Button
          classes={{ label: classes.buttonLabel }}
          className={classes.buttonTwo}
          fullWidth
          variant="text"
          color="primary"
          onClick={() => {
            dispatch({ type: SIGNIN })
          }}
        >
          ورود به سایت
        </Button>
      </div>
    </Modal>
  )
}
