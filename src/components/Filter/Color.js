import React, { useState } from 'react'
import useStyles from './color.style'
import { Typography, Collapse, Button } from '@material-ui/core'
import ColorItem from './ColorItem'

export default function Color() {
  const [open, setOpen] = useState(false)
  const [data, setData] = useState([
    { id: 0, color: '#f44336' },
    { id: 1, color: '#2196f3' },
    { id: 2, color: '#e91e63' },
    { id: 3, color: '#9c27b0' },
    { id: 4, color: '#673ab7' },
    { id: 5, color: '#3f51b5' },
    { id: 6, color: '#03a9f4' },
    { id: 7, color: '#00bcd4' },
    { id: 8, color: '#212121' },
    { id: 9, color: '#4acf50' },
    { id: 10, color: '#009688' },
    { id: 11, color: '#8bc34a' },
    { id: 12, color: '#cddc39' },
    { id: 13, color: '#ffeb3b' },
    { id: 14, color: '#ff5722' }
  ])
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Collapse collapsedHeight={54} in={open}>
        <div
          className={classes.titleDiv}
          onClick={() => setOpen(!open)}
          style={{
            borderBottomLeftRadius: open ? 0 : 5,
            borderBottomRightRadius: open ? 0 : 5
          }}
        >
          <Typography>انتخاب رنگ</Typography>
        </div>
        <div className={classes.itemsDiv}>
          {data.map((item) => (
            <ColorItem key={item.id} color={item.color} />
          ))}
        </div>
        <div className={classes.btnDiv}>
          <Button variant="outlined" color="primary">
            <Typography variant="body2"> اعمال فیلتر</Typography>
          </Button>
          <Button variant="outlined" color="secondary">
            <Typography variant="body2"> همه رنگها</Typography>
          </Button>
        </div>
      </Collapse>
    </div>
  )
}
