import React, { useState } from 'react'
import useStyles from './size.styles'
import {
  FormControlLabel,
  Checkbox,
  Typography,
  Collapse,
  Button
} from '@material-ui/core'

export default function Size() {
  const [open, setOpen] = useState(false)
  const classes = useStyles()
  const [sizes, setSizes] = useState([
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43,
    44,
    45
  ])
  return (
    <div className={classes.root}>
      <Collapse collapsedHeight={54} in={open}>
        <div
          className={classes.titleDiv}
          onClick={() => setOpen(!open)}
          style={{
            borderBottomLeftRadius: open ? 0 : 5,
            borderBottomRightRadius: open ? 0 : 5
          }}
        >
          <Typography>انتخاب سایز کفش</Typography>
        </div>
        <div className={classes.itemsDiv}>
          {sizes.map((item) => (
            <FormControlLabel
              style={{ width: 50 }}
              control={
                <Checkbox
                  // checked={state.checkedB}
                  // onChange={handleChange}
                  name="checkedB"
                  color="secondary"
                />
              }
              label={item}
            />
          ))}
        </div>
        <div className={classes.btnDiv}>
          <Button variant="outlined" color="primary">
            <Typography variant="body2"> اعمال فیلتر</Typography>
          </Button>
          <Button variant="outlined" color="secondary">
            <Typography variant="body2"> همه اندازه ها</Typography>
          </Button>
        </div>
      </Collapse>
    </div>
  )
}
