import { makeStyles } from '@material-ui/core'

export default makeStyles((theme) => ({
  root: {
    width: '100%',
    height: 60,
    display: 'flex',
    flexDirection: 'row',
    // paddingRight: 10,
    // paddingLeft: 10,
    padding: 10,
    alignItems: 'center',
    backgroundColor: '#131a21',
    boxShadow: '1px 0px 3px 0px #3b3939'
  },
  logoDiv: {
    width: 200
  },
  logoImg: {
    width: '90%'
  },
  menuDiv: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    height: '100%'
  },
  activeMenu: {
    color: theme.palette.text.primary
  },
  inactiveMenu: {
    color: theme.palette.text.inavctiveMenu
  },
  activeMenuDiv: {
    borderBottomColor: '#00c0c9',
    // borderRadius: 5,
    borderBottomWidth: 1,
    borderBottomStyle: 'solid'
    // height: '100%',
    // alignItems: 'center',
    // display: 'flex'
  },
  leftMenuDiv: {
    height: '100%',
    // width: 160,
    paddingRight: 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
}))
