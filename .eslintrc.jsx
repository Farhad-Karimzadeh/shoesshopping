{
  "extends": [
    "airbnb",
    "airbnb/hooks",
    "plugin:prettier/recommended",
    "prettier/react",
    "prettier/standard"
  ],
  "plugins": ["prettier"],
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    },
    "ecmaVersion": 2018
  },
  "rules": {
    
      "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    
    "react/jsx-filename-extensions": [
      1,
      {
        "extensions": [".js", ".jsx"]
      }
    ],
    "react/prop-types": 0,
    "import/imports-first": ["error", "absolute-first"],
    "import/newline=after-import": "error",
    "import/prefer-defualt-export": "off",
    "react/jsx-props-no-spreading": "off"
  },
  "globals": {
    "windows": true,
    "document": true,
    "localStorage": true,
    "FormData": true,
    "FileReader": true,
    "Blob": true,
    "navigator": true,
    "fetch": true
  }
}
