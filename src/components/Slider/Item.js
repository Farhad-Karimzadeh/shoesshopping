import { Typography, Button } from '@material-ui/core'
import React from 'react'
import useStyles from './item.style'

export default function Item({ item }) {
  //item= item.title item.image item.price item.discount item.subtitle
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <div className={classes.rightSlider}>
        <img src={item.image} alt={item.title} className={classes.image} />
      </div>
      <div className={classes.leftSlider}>
        <Typography variant="h2">{item.title}</Typography>
        <Typography className={classes.subtitle} variant="subtitle1">
          {item.subtitle}
        </Typography>
        <Typography className={classes.subtitle} variant="body1">
          {item.price} تومان
        </Typography>
        <div className={classes.discountDiv}>
          <Typography className={classes.discount} variant="subtitle2">
            {item.discount} % تخفیف
          </Typography>
        </div>
      </div>
      <div className={classes.buttonDiv}>
        <Button variant="outlined" color="primary">
          مشاهده جزییات
        </Button>
      </div>
    </div>
  )
}
