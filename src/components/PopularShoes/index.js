import React from 'react'
import { Grid } from '@material-ui/core'
import Title from './Title'
import Mens from './Mens'
import Womens from './Womens'
import Kids from './Kids'
import LazyLoad from 'react-lazyload'
export default function Index() {
  return (
    <>
      <LazyLoad height={365}>
        <Grid container style={{ height: 365 }}>
          <Title title="پرفروش های مردانه" />
          <Mens />
        </Grid>
      </LazyLoad>
      <LazyLoad height={365}>
        <Grid container style={{ height: 365 }}>
          <Title title="پرفروش های زنانه" />
          <Womens />
        </Grid>
      </LazyLoad>
      <LazyLoad height={365} offset={100}>
        {' '}
        {/* offset به این معناست که 100 پیکسل مانده به کامپوننت آنرا لود می کنه */}
        <Grid container style={{ marginBottom: 30, height: 365 }}>
          <Title title="پرفروش های بچگانه" />
          <Kids />
        </Grid>
      </LazyLoad>
    </>
  )
}
