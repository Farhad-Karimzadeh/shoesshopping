import React from 'react'
import useStyles from './index.style'
import { Container, Grid, Typography, Button } from '@material-ui/core'
import TitleComponent from '../titleWithBottomBorder'

export default function Index() {
  const classes = useStyles()
  return (
    <Grid className={classes.root} direction="row" container>
      <Container maxWidth="lg">
        <Grid className={classes.root} container direction="row">
          <Grid className={classes.item} item xs={12} lg={3}>
            <TitleComponent title=" فروشگاه کفش" />
            <Typography
              className={classes.address}
              variant="body2"
              color="textSecondary"
            >
              تهران، میدان بهارستان بعد از چهار راه سرچشمه کوچه نصراللهی کوچه
              فخرالملک پلاک 23 واحد3 ساختمان آهو
            </Typography>
            <Typography
              className={classes.address}
              variant="body2"
              color="textSecondary"
            >
              تلفن تماس: 09121763640
            </Typography>
          </Grid>
          <Grid className={classes.item} item xs={12} lg={3}>
            <TitleComponent title=" ویژگیها" />
            <Typography
              className={classes.address}
              variant="body2"
              color="textSecondary"
            >
              ارسال رایگان
            </Typography>{' '}
            <Typography
              className={classes.address}
              variant="body2"
              color="textSecondary"
            >
              برنــدهای اصلی
            </Typography>{' '}
            <Typography
              className={classes.address}
              variant="body2"
              color="textSecondary"
            >
              پایین تر از قیمت بازار
            </Typography>
            <Typography
              className={classes.address}
              variant="body2"
              color="textSecondary"
            >
              امکان برگشت محصول تا هفت روز
            </Typography>{' '}
          </Grid>

          <Grid className={classes.item} item xs={12} lg={3}>
            <TitleComponent title=" برندها" />
            <Button className={classes.brandButton} variant="text">
              <Typography
                className={classes.address}
                variant="body2"
                color="primary"
              >
                آدیداس
              </Typography>
            </Button>
            <Button className={classes.brandButton} variant="text">
              <Typography
                className={classes.address}
                variant="body2"
                color="primary"
              >
                نایک{' '}
              </Typography>
            </Button>
            <Button className={classes.brandButton} variant="text">
              <Typography
                className={classes.address}
                variant="body2"
                color="primary"
              >
                پوما
              </Typography>
            </Button>
          </Grid>
          <Grid className={classes.item} item xs={12} lg={3}>
            <TitleComponent title=" لینکهای مفید" />
            <Button className={classes.brandButton} variant="text">
              <Typography
                className={classes.address}
                variant="body2"
                color="primary"
              >
                سوالات پر تکرار
              </Typography>
            </Button>
            <Button className={classes.brandButton} variant="text">
              <Typography
                className={classes.address}
                variant="body2"
                color="primary"
              >
                بلاگ
              </Typography>
            </Button>
            <Button className={classes.brandButton} variant="text">
              <Typography
                className={classes.address}
                variant="body2"
                color="primary"
              >
                تماس با ما
              </Typography>
            </Button>
          </Grid>
        </Grid>
      </Container>
    </Grid>
  )
}
