import React, { useState } from 'react'

export default function ColorItem({ color }) {
  const [active, setActive] = useState(false)
  return (
    <div
      style={{
        backgroundColor: color,
        width: 35,
        height: 35,
        cursor: 'pointer',
        borderStyle: 'solid',
        // borderColor: '#6767cc', activeColor d8d8ff,not activeColor 3e3ead
        borderColor: active ? '#d8d8ff' : '#3e3ead',
        borderWidth: active ? 2 : 0.5,
        borderRadius: 30,
        margin: 8
      }}
      onClick={() => {
        setActive(!active)
      }}
    />
  )
}
